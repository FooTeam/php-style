<?php

require __DIR__.'/vendor/autoload.php';

return FooTeam\styles(PhpCsFixer\Finder::create()
    ->in(__DIR__.'/src'));
