# FooTeam's styling rules for PHP-CS-Fixer

Rules preset :
- `@Laravel` by [Matt Allan](https://mattallan.me/posts/automate-code-formatting-for-laravel-projects/)
- `@Laravel:risky` by [Matt Allan](https://mattallan.me/posts/automate-code-formatting-for-laravel-projects/)
- [`@PHP80Migration`](https://github.com/FriendsOfPHP/PHP-CS-Fixer/blob/2.18/doc/ruleSets/PHP80Migration.rst)
- [`@PHP80Migration:risky`](https://github.com/FriendsOfPHP/PHP-CS-Fixer/blob/2.18/doc/ruleSets/PHP80MigrationRisky.rst)

And custom rules (see [`/src/rules`](https://gitlab.com/FooTeam/php-style/-/blob/main/src/rules.php)).

## Installation
Add this repository in your composer.json:
```
"repositories": [
  {
    "type": "vcs",
    "url": "https://gitlab.com/FooTeam/php-style"
  }
]
```

Then execute the following commands:
``` bash
composer require footeam/php-style:dev-main --dev

echo "<?php

require __DIR__.'/vendor/autoload.php';

return FooTeam\styles(PhpCsFixer\Finder::create()
    ->in([__DIR__.'/app', __DIR__.'/config', __DIR__.'/database', __DIR__.'/routes', __DIR__.'/tests']));
" >> .php-cs-fixer.dist.php
```

Don't forget to add the following line to the `.gitignore`:
```
.php-cs-fixer.cache
```

## Usage
In a terminal:
``` bash
./vendor/bin/php-cs-fixer fix
```

In a git pre-commit hook (`.git/hooks/pre-commit`):
```bash
#!/usr/bin/env bash

source_root=$(git rev-parse --show-toplevel)
staged_files=$(git diff --cached --name-only)
exit_code=0

for staged in ${staged_files}; do
    if [[ -f ${staged} && ${staged} == *.php ]]; then
		${source_root}/vendor/bin/php-cs-fixer fix ${staged} --dry-run -vv --using-cache=no

		if [[ $? -ne 0 ]]; then
		    exit_code=1
		fi
    fi
done

if [[ ${exit_code} -eq 1 ]]; then
    echo "lint failed"
fi

exit ${exit_code}
```

## Risky rules
This will apply the following risky rules:
- no_php4_constructor
- no_alias_functions
- implode_call
- combine_nested_dirname
- pow_to_exponentiation
- void_return
- is_null
- no_unreachable_default_argument_value
- self_accessor
- no_unneeded_final_method
- use_arrow_functions
- non_printable_character
- random_api_migration
- psr4
- psr_autoloading

Check them [here](https://github.com/FriendsOfPHP/PHP-CS-Fixer/blob/2.18/doc/rules/index.rst).
