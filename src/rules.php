<?php

// Updated at v2.18.4 Remote Void

return [
    '@PHP80Migration' => true,
    '@PHP80Migration:risky' => true,

    'declare_strict_types' => false, // Overwrite value from @PHP80Migration:risky

    'combine_consecutive_issets' => true,
    'combine_consecutive_unsets' => true,
    'explicit_indirect_variable' => true,
    'explicit_string_variable' => true,
    'simple_to_complex_string_variable' => true,
    'method_argument_space' => ['on_multiline' => 'ensure_fully_multiline'],
    'no_superfluous_elseif' => true,
    'no_useless_else' => true,
    'operator_linebreak' => ['only_booleans' => true],
    'ordered_class_elements' => true,
    'return_assignment' => true,
    'single_line_throw' => true,
    'fully_qualified_strict_types' => true,
    'is_null' => true, // risky if is_null is overridden
    'yoda_style' => true,

    // PHPDoc
    'multiline_comment_opening_closing' => true,
];
