<?php

namespace FooTeam;

use MattAllan\LaravelCodeStyle\Config as Laravel;
use PhpCsFixer\Config;
use PhpCsFixer\ConfigInterface;
use PhpCsFixer\Finder;

function styles(Finder $finder): ConfigInterface
{
    return (new Config())
        ->setFinder($finder)
        ->setRiskyAllowed(true)
        ->setRules(array_merge(
            Laravel::RULE_DEFINITIONS['@Laravel'],
            Laravel::RULE_DEFINITIONS['@Laravel:risky'],
            require __DIR__.'/rules.php'
        ));
}
